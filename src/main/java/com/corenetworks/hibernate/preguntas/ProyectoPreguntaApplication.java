package com.corenetworks.hibernate.preguntas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProyectoPreguntaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProyectoPreguntaApplication.class, args);
	}
}
