package com.corenetworks.hibernate.preguntas.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.corenetworks.hibernate.preguntas.model.Profesor;
import com.corenetworks.hibernate.preguntas.beans.LoginBean;
import com.corenetworks.hibernate.preguntas.dao.ProfesorDao;

@Controller
public class LoginController {

	@Autowired
	private ProfesorDao profesorDao;

	@Autowired
	private HttpSession httpSession;

	@GetMapping(value = "/signin")
	public String entrar(Model modelo) {
		modelo.addAttribute("profesorLogin", new LoginBean());
		return "login";
	}

	@PostMapping(value = "/login")
	public String submit(@ModelAttribute("profesorLogin") LoginBean l, Model modelo) {
		Profesor pr = profesorDao.getByEmailAndPassword(l.getEmail(), l.getPassword());
		if (pr != null) {
			httpSession.setAttribute("profesorLoggedIn", pr);
			httpSession.setMaxInactiveInterval(0);
			return "redirect:/";
		} else {
			modelo.addAttribute("error", "Error al introducir el usuario o la contraseña");
			return "login";
		}

	}

	@GetMapping(value = "/logout")
	public String salir(Model modelo) {
		httpSession.removeAttribute("profesorLoggedIn");
		return "redirect:/";
	}
}
