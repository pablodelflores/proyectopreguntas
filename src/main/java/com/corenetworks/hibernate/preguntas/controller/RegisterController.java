package com.corenetworks.hibernate.preguntas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.corenetworks.hibernate.preguntas.beans.RegisterBean;
import com.corenetworks.hibernate.preguntas.dao.ProfesorDao;
import com.corenetworks.hibernate.preguntas.model.Profesor;

@Controller
public class RegisterController {
	
	@Autowired
	private ProfesorDao profesorDao;
	
	@GetMapping(value="/signup")
	public String registrate(Model modelo){
		modelo.addAttribute("profesorRegister", new RegisterBean());
		return "registro";
	}
	
	@PostMapping(value="/register")
	public String submit(@ModelAttribute("profesorRegister") RegisterBean r, Model modelo){
		profesorDao.create(new Profesor(
					r.getNombre(),
					r.getEmail(),
					r.getPassword()
				));
		return "redirect:/profesores";
	}
}
