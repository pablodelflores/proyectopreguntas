package com.corenetworks.hibernate.preguntas.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import com.corenetworks.hibernate.preguntas.beans.PreguntaBean;
import com.corenetworks.hibernate.preguntas.beans.RespuestaBean;
import com.corenetworks.hibernate.preguntas.dao.PreguntaDao;
import com.corenetworks.hibernate.preguntas.dao.RespuestaDao;
import com.corenetworks.hibernate.preguntas.model.Pregunta;
import com.corenetworks.hibernate.preguntas.model.Profesor;
import com.corenetworks.hibernate.preguntas.model.Respuesta;

@Controller
public class PreguntaController {

	@Autowired
	private PreguntaDao preguntaDao;
	@Autowired
	private HttpSession httpSession;
	@Autowired
	private RespuestaDao respuestaDao;
	
	@GetMapping(value="/")
	public String listaPreguntas(Model modelo) {
		modelo.addAttribute("pregunta", preguntaDao.getAll());
		return "index";
	}
	
	@GetMapping(value="/submit")
	public String showForm(Model modelo) {
		modelo.addAttribute("pregunta", new PreguntaBean());
		return "submit";
	}
	
	@PostMapping(value="/submit/newquestion")
	public String submit(@ModelAttribute("pregunta") PreguntaBean p, Model modelo){
		
		Pregunta pregunta = new Pregunta();
		
		pregunta.setTitulo(p.getTitulo());
		pregunta.setContenido(p.getContenido());
		pregunta.setCodigo(p.getCodigo());
		pregunta.setCategoria(p.getCategoria());
		
		Profesor profesor = (Profesor) httpSession.getAttribute("userLoggedIn");
		pregunta.setProfesor(profesor);
		
		preguntaDao.create(pregunta);
		
		return "redirect:/";
	}
	
	@GetMapping(value="/pregunta/{id}")
	public String detail(@PathVariable("id") long id, Model modelo) {
		
		Pregunta result = null;
		if ((result = preguntaDao.getById(id)) != null) {
			modelo.addAttribute("pregunta", result);
			modelo.addAttribute("respuestaForm", new RespuestaBean());
			return "pregunta";
		} else {
			return "redirect:/";
		}
	}
	
	@PostMapping(value="/submit/newRespuesta")
	public String submitRespuesta(@ModelAttribute("respuestaForm") RespuestaBean respuestaBean, Model model) {
		Profesor profesor = (Profesor) httpSession.getAttribute("userLoggedIn");
		
		Respuesta respuesta = new Respuesta();
		respuesta.setProfesor(profesor);
		
		Pregunta pregunta = preguntaDao.getById(respuestaBean.getPost_id());
		respuesta.setPregunta(pregunta);
		respuesta.setContenido(respuestaBean.getContenido());
		respuestaDao.create(respuesta);
		pregunta.getRespuestas().add(respuesta);
		profesor.getRespuesta().add(respuesta);
		
		return "redirect:/pregunta/"+ respuestaBean.getPost_id();
	}
}
