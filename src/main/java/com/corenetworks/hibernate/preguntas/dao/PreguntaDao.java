package com.corenetworks.hibernate.preguntas.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;
import com.corenetworks.hibernate.preguntas.model.Pregunta;

@Repository
@Transactional
public class PreguntaDao {

	@PersistenceContext
	private EntityManager entityManager;
	
	public void create(Pregunta pregunta) {
		entityManager.persist(pregunta);
		return;
	}
	
	@SuppressWarnings("unchecked")
	public List<Pregunta> getAll(){
		return entityManager
				.createQuery("select pr from Pregunta pr")
				.getResultList();
	}
	
	public Pregunta getById(long id) {
		return entityManager.find(Pregunta.class, id);
	}
}
