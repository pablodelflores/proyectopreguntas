package com.corenetworks.hibernate.preguntas.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.corenetworks.hibernate.preguntas.model.Respuesta;



@Repository
@Transactional
public class RespuestaDao {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	public void create(Respuesta respuesta) {
		entityManager.persist(respuesta);
		return;
	}
	
	@SuppressWarnings("uncheked")
	public List<Respuesta> getAll() {
		return entityManager
				.createQuery("select res from Respuesta eres")
				.getResultList();
	}
	
	public Respuesta getById(long id) {
		return entityManager.find(Respuesta.class, id);
	}

}
