package com.corenetworks.hibernate.preguntas.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.CreationTimestamp;


@Entity
public class Pregunta {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@Column
	@Lob
	private String titulo;
	
	@Column
	@Lob
	private String contenido;
	
	@Column
	@Lob
	private String codigo;
	
	@Column
	@CreationTimestamp
	private Date fecha;
	
	@Column
	private String categoria;
	
	@ManyToOne
	private Profesor profesor;
	
	@OneToMany(mappedBy="pregunta")
	private List<Respuesta> respuestas = new ArrayList<>();

	public List<Respuesta> getRespuestas() {
		return respuestas;
	}

	public void setRespuestas(List<Respuesta> respuestas) {
		this.respuestas = respuestas;
	}

	public Pregunta() {
	}

	public Pregunta(String titulo, String contenido, Profesor profesor, String categoria, String codigo) {
		this.titulo = titulo;
		this.contenido = contenido;
		this.profesor = profesor;
		this.categoria = categoria;
		this.codigo = codigo;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getContenido() {
		return contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Profesor getProfesor() {
		return profesor;
	}

	public void setProfesor(Profesor profesor) {
		this.profesor = profesor;
	}
	
	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	@Override
	public String toString() {
		return "Pregunta [id=" + id + ", titulo=" + titulo + ", contenido=" + contenido + ", codigo=" + codigo
				+ ", fecha=" + fecha + ", categoria=" + categoria + ", profesor=" + profesor + "]";
	}

}
