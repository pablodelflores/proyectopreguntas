package com.corenetworks.hibernate.preguntas.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.annotations.CreationTimestamp;

//import com.jcraft.jsch.jce.MD5;

@Entity
public class Profesor {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@Column
	private String nombre;
	
	@Column
	private String email;
	
	@Column
	@CreationTimestamp
	private Date fechaAlta;
	
	@Column
	@ColumnTransformer(write=" MD5(?) ")
	private String password;
	
	@OneToMany(mappedBy="profesor", fetch= FetchType.EAGER)
	private Set<Respuesta> respuesta = new HashSet<>();
	
	

	public Set<Respuesta> getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(Set<Respuesta> respuesta) {
		this.respuesta = respuesta;
	}

	public Profesor() {
		super();
	}

	public Profesor(String nombre, String email, String password) {
		this.nombre = nombre;
		this.email = email;
		this.password = password;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Profesor [id=" + id + ", nombre=" + nombre + ", email=" + email + ", fechaAlta=" + fechaAlta
				+ ", password=" + password + "]";
	}
	
}
