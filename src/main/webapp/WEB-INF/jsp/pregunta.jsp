<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="author" content="Pablo Delgado Flores">
<meta name="author" content="lk2_89">
<meta name="description"
	content="Blog creado con Spring Boot, Spring MVC, JPA e Hibernate">
<title>Proyecto Spring Preguntas y Respuestas</title>
<link href="../webjars/bootstrap/3.3.7-1/css/bootstrap.min.css"
	rel="stylesheet">
<link href="../assets/css/profile.css"ssets/css/profile.css" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Hind" rel="stylesheet"
	type="text/css">
<link href="//fonts.googleapis.com/css?family=Open+Sans"
	rel="stylesheet" type="text/css">
<link href="//fonts.googleapis.com/css?family=Montserrat"
	rel="stylesheet" type="text/css">
<link
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
	rel="stylesheet"
	integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
	crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<div class="header clearfix">
			<!-- <nav>
			<ul class="nav nav-pills pull-right"> -->
			<c:choose>
				<c:when test="${not empty sessionScope.userLoggedIn}">
					<jsp:include page="includes/menu_logged.jsp">
						<jsp:param value="inicio" name="inicio" />
						<jsp:param value="${sessionScope.userLoggedIn.nombre}"
							name="usuario" />
					</jsp:include>
				</c:when>
				<c:otherwise>
					<jsp:include page="includes/menu.jsp">
						<jsp:param value="inicio" name="inicio" />
					</jsp:include>
				</c:otherwise>
			</c:choose>
			<!-- </ul>
		</nav> -->
			<!-- <h3 class="text-mutted">Pregúntame</h3> -->
		</div>


		<!-- <div class="container" style="margin-top: 8em;">
			<h1>Preguntas y Respuestas</h1>
		</div>-->
		<div class="row">
			<div class="col-md-12 col-lg-12" style="margin-top: 8em;">
				<h1>${pregunta.titulo}</h1>
				<div>
					<div class="pull-right" style="padding: 10px 0 0 5px;">${pregunta.profesor.nombre}</div>
					<img alt="User Pic"
						src="http://i.pravatar.cc/50?u=${pregunta.profesor.email}"
						class="img-circle img-responsive pull-right">
					<p></p>
				</div>
				<div style="clear: both; margin-bottom: 10px;"></div>
				<p>${pregunta.contenido}</p>
				<div>
					<span class="badge">Escrito el <fmt:formatDate
							pattern="dd/MM/yyyy" value="${pregunta.fecha}" /> a las <fmt:formatDate
							pattern="HH:mm:ss" value="${pregunta.fecha}" /></span><span
						class="label label-info" style="margin-left: 10px;">
						${(fn:length(pregunta.respuestas) gt 0) ? fn:length(pregunta.respuestas) : 'Sin '}
						${(fn:length(pregunta.respuestas) == 1) ? 'respuesta' : 'respuestas'}
					</span>
				</div>
				<hr>
			</div>
		</div>

		<div class="row comment">
			<div class="col-md-12 col-lg-12">
				<div class="well">

					<h4>Respuestas</h4>
					<c:choose>
						<c:when test="${not empty sessionScope.userLoggedIn}">

							<form:form method="POST" modelAttribute="commentForm"
								id="form-comment" action="/submit/newComment" role="form">
								<form:input type="hidden" id="post_id" name="post_id"
									path="post_id" value="${pregunta.id}" />
								<div class="input-group">
									<form:input type="text"
										class="form-control input-sm chat-input"
										placeholder="Escribe tu comentario aquí" path="contenido" />
									<span class="input-group-btn" id="comment-button"> <a
										href="#" class="btn btn-primary btn-sm"> <span
											class="glyphicon glyphicon-comment"></span> Comentar
									</a>
									</span>
								</div>
							</form:form>

						</c:when>
						<c:otherwise>
							<h5>Necesitas iniciar sesión para añadir la respuesta a esta pregunta</h5>
						</c:otherwise>
					</c:choose>
					<hr data-brackets-id="12673">
					<ul data-brackets-id="12674" id="sortable"
						class="list-unstyled ui-sortable">

						<c:if test="${not empty pregunta.respuestas }">
							<c:forEach items="${pregunta.respuestas}" var="respuesta">
								<li class="ui-state-default"><strong
									class="pull-left primary-font">${respuesta.profesor.nombre}</strong>
									<small class="pull-right text-muted"> <span
										class="glyphicon glyphicon-time"></span> <fmt:formatDate
											pattern="dd/MM/yyyy" value="${respuesta.fecha}" /> a las <fmt:formatDate
											pattern="HH:mm:ss" value="${respuesta.fecha}" />
								</small> <br /> ${respuesta.contenido} <br /></li>
							</c:forEach>
						</c:if>

					</ul>
				</div>
			</div>
		</div>

		<div>

			<footer class="footer"> </footer>

		</div>
		<!-- /container -->
		<script src="/webjars/jquery/3.1.1/jquery.min.js"></script>
		<script src="/webjars/bootstrap/3.3.7-1/js/bootstrap.min.js"></script>
		<script>
			$(document).ready(function() {
				$("#comment-button").click(function() {
					$("#form-comment").submit();
				});
			});
		</script>
</body>
</html>