<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="author" content="Pablo Delgado Flores">
<meta name="author" content="lk2_89">
<meta name="description"
	content="Blog creado con Spring Boot, Spring MVC, JPA e Hibernate">
<title>Proyecto Spring Preguntas y Respuestas</title>

<link href="webjars/bootstrap/3.3.7-1/css/bootstrap.min.css"
	rel="stylesheet">

<link href="assets/css/profile.css" rel="stylesheet">
</head>
<body>
	<div class="container">
		<div class="header clearfix">
			<!-- HEADER + MENU -->
			<c:choose>
				<c:when test="${not empty sessionScope.userLoggedIn}">
					<jsp:include page="includes/menu_logged.jsp">
						<jsp:param value="profesores" name="profesores" />
						<jsp:param value="${sessionScope.userLoggedIn.nombre}"
							name="usuario" />
					</jsp:include>
				</c:when>
				<c:otherwise>
					<jsp:include page="includes/menu.jsp">
						<jsp:param value="profesores" name="profesores" />
					</jsp:include>
				</c:otherwise>
			</c:choose>
			<!--  /HEADER -->
			<!-- <h3 class="text-mutted">Pregúntame</h3> -->
		</div>

		<div class="container" style="margin-top: 8em;">
			<c:forEach items="${profesores}" var="profesor">


				<div class="row">
					<div class="col-lg-5">
						<div class="media">
							<a class="pull-left" href="#"> <img
								class="media-object dp img-circle"
								src="http://i.pravatar.cc/150?u=${profesor.email}"
								style="width: 100px; height: 100px;">
							</a>
							<div class="media-body">
								<h4 class="media-heading">${profesor.nombre}</h4>
								<h5>Email: ${profesor.email}</h5>
								<h6 style="color: #337ab7; font-weight: bold;">
									Se dio de alta el:
									<fmt:formatDate pattern="dd/MM/yyyy"
										value="${profesor.fechaAlta}" />
								</h6>
								<hr style="margin: 8px auto">

								<!--  <span class="label label-default">HTML5/CSS3</span> <span
									class="label label-default">jQuery</span> <span
									class="label label-info">CakePHP</span> <span
									class="label label-default">Android</span> -->
							</div>
						</div>

					</div>
				</div>




			</c:forEach>
		</div>
	</div>
	<script src="webjars/jquery/3.1.1/jquery.min.js"></script>
	<script src="webjars/bootstrap/3.3.7-1/js/bootstrap.min.js"></script>
</body>
</html>