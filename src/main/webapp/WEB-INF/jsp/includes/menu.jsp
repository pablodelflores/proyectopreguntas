
<div class="navbar-wrapper">
	<div class="container-fluid">
		<nav class="navbar navbar-fixed-top">
			<div class="container">
				 <div class="navbar-header">
					<!--<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button> -->
					<h3 class="text-mutted">Pregúntame</h3>
					
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<a class="navbar-brand" href="/" style="padding-left: 2em;"><img
						src="../assets/images/question-circle-512.png" width="30px"
						height="auto" /></a>
					<ul class="nav navbar-nav pull-right">
						
						<ul class="nav navbar-nav">
							<li role="presentation"
								${not empty param.inicio ? 'class="active"' : ''}><a
								href="/">Inicio</a></li>
							<li role="presentation"
								${not empty param.profesores ? 'class="active"' : ''}><a
								href="/profesores">Profesores</a></li>
						</ul>
						
						<li class=" dropdown"><a href="#"
							class="dropdown-toggle active" data-toggle="dropdown"
							role="button" aria-haspopup="true" aria-expanded="false">Usuario
								<span class="caret"></span>
						</a>
							<ul class="dropdown-menu">
								<li><a href="/signup">Registrate</a></li>
								<li><a href="/signin">Login</a></li>
							</ul></li>
					</ul>
				</div>
			</div>
		</nav>
	</div>
</div>