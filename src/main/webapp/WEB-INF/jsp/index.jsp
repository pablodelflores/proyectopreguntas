<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="author" content="Pablo Delgado Flores">
<meta name="author" content="lk2_89">
<meta name="description"
	content="Blog creado con Spring Boot, Spring MVC, JPA e Hibernate">
<title>Proyecto Spring Preguntas y Respuestas</title>
<link href="webjars/bootstrap/3.3.7-1/css/bootstrap.min.css"
	rel="stylesheet">
<link href="assets/css/profile.css" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Hind" rel="stylesheet"
	type="text/css">
<link href="//fonts.googleapis.com/css?family=Open+Sans"
	rel="stylesheet" type="text/css">
<link href="//fonts.googleapis.com/css?family=Montserrat"
	rel="stylesheet" type="text/css">
<link
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
	rel="stylesheet"
	integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
	crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<div class="header clearfix">
			<!-- <nav>
			<ul class="nav nav-pills pull-right"> -->
			<c:choose>
				<c:when test="${not empty sessionScope.userLoggedIn}">
					<jsp:include page="includes/menu_logged.jsp">
						<jsp:param value="inicio" name="inicio" />
						<jsp:param value="${sessionScope.userLoggedIn.nombre}"
							name="usuario" />
					</jsp:include>
				</c:when>
				<c:otherwise>
					<jsp:include page="includes/menu.jsp">
						<jsp:param value="inicio" name="inicio" />
					</jsp:include>
				</c:otherwise>
			</c:choose>
			<!-- </ul>
		</nav> -->
			<!-- <h3 class="text-mutted">Pregúntame</h3> -->
		</div>


		<!-- <div class="container" style="margin-top: 8em;">
			<h1>Preguntas y Respuestas</h1>
		</div>-->
		<div class="jumbotron jumbotron-muted" style="margin-top: 8em;">
			<div class="container">
				<div class="col-lg-6">
					<h2 align="center" style="font-weight: bold;">Preguntas y
						respuestas</h2>
					<p class="lead">Regístrate para poder realizar tus exámenes y
						poner a prueba tus conocimientos... ¡Aprende!</p>
					<div class="input-group">
						<span class="input-group-btn">
							<button class="btn btn-primary" type="button"
								style="background-color: #048ede;">¡Apúntame!</button>
						</span>
					</div>
					<br> <small>By submitting this form, you agree to our
						<a class="link-white" href="#!" target="_blank">Terms of
							Service <i class="fa fa-external-link" aria-hidden="true"></i>
					</a> and <a class="link-white" href="#!" target="_blank">Privacy
							Policy <i class="fa fa-external-link" aria-hidden="true"></i>
					</a>.
					</small>
				</div>
				<div class="col-lg-6">
					<img class="img-responsive"
						src="https://ussolarinstitute.com/wp-content/uploads/2016/05/Frequently-Asked-Questions.png" />
				</div>
			</div>
		</div>
		
		<c:forEach items="${pregunta}" var="pregunta">
		<div class="col-md-12">
			
				<h3><a href="/pregunta/${pregunta.id}">Pregunta nº ${pregunta.id}</a></h3>
				<blockquote>${pregunta.titulo}</br></br>
				<blockquote>${pregunta.contenido}</br></br>
				<code>${pregunta.codigo}</code></blockquote>
				<span class="label label-info">${pregunta.categoria}</span>
			
		</div>
		</c:forEach>


	</div>
	<script src="webjars/jquery/3.1.1/jquery.min.js"></script>
	<script src="webjars/bootstrap/3.3.7-1/js/bootstrap.min.js"></script>
</body>
</html>