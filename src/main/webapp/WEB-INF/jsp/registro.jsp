<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="author" content="Pablo Delgado Flores">
<meta name="author" content="lk2_89">
<meta name="description"
	content="Blog creado con Spring Boot, Spring MVC, JPA e Hibernate">
<title>Proyecto Spring Preguntas y Respuestas</title>
<link href="webjars/bootstrap/3.3.7-1/css/bootstrap.min.css"
	rel="stylesheet">
<link href="assets/css/profile.css" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Hind" rel="stylesheet"
	type="text/css">
<link href="//fonts.googleapis.com/css?family=Open+Sans"
	rel="stylesheet" type="text/css">
<link href="//fonts.googleapis.com/css?family=Montserrat"
	rel="stylesheet" type="text/css">
<link
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
	rel="stylesheet"
	integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
	crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<div class="header clearfix">
			<!-- <nav>
			<ul class="nav nav-pills pull-right"> -->
			<c:choose>
				<c:when test="${not empty sessionScope.userLoggedIn}">
					<jsp:include page="includes/menu_logged.jsp">
						<jsp:param value="inicio" name="inicio" />
						<jsp:param value="${sessionScope.userLoggedIn.nombre}"
							name="usuario" />
					</jsp:include>
				</c:when>
				<c:otherwise>
					<jsp:include page="includes/menu.jsp">
						<jsp:param value="inicio" name="inicio" />
					</jsp:include>
				</c:otherwise>
			</c:choose>
			<!-- </ul>
		</nav> -->
			<!-- <h3 class="text-mutted">Pregúntame</h3> -->
		</div>


		<!-- <div class="container" style="margin-top: 8em;">
			<h1>Preguntas y Respuestas</h1>
		</div>-->
			
			<div class="col-lg-8 col-lg-offset-2" style="margin-top: 8em;">
    		<div class="text-center">
  			<h3>Registro</h3>
  			<form:form id="register-form" action="/register" method="post" role="form" autocomplete="off" modelAttribute="profesorRegister">
  				<div class="form-group">
  					<form:input type="text" name="nombre" id="nombre" tabindex="1" class="form-control" path="nombre" placeholder="Nombre" />
  				</div>	
 				<div class="form-group">	
  					<form:input type="email" name="email" id="email" tabindex="2" class="form-control" path="email" placeholder="Correo electrónico" />
  				</div>	
  				
  				<div class="form-group">	
  					<form:input type="password" name="password" id="password" tabindex="4" class="form-control" path="password" placeholder="Contraseña" />
  				</div>	
  				<div class="form-group">	
  					<form:input type="password" name="password2" id="password2" tabindex="5" class="form-control" path="secondPassword" placeholder="Repetir contraseña" data-rule-equalTo="#password"/>
  				</div>	
  				<div class="form-group">	
  					<input type="submit" name="register-submit" id="register-submit" tabindex="6" class="form-control btn btn-info" value="Registrar ahora" />
  				</div>
  			
  			</form:form>
    		</div>
    		
    </div>


	</div>
	<script src="webjars/jquery/3.1.1/jquery.min.js"></script>
	<script src="webjars/bootstrap/3.3.7-1/js/bootstrap.min.js"></script>
</body>
</html>